FROM openjdk:12-alpine

COPY ./build/libs/cars-api.jar .

CMD ["java", "-jar", "cars-api.jar"]
